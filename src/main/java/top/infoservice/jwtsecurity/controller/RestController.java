package top.infoservice.jwtsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import top.infoservice.jwtsecurity.dto.JwtRequest;
import top.infoservice.jwtsecurity.dto.JwtResponse;
import top.infoservice.jwtsecurity.dto.RefreshJwtRequest;
import top.infoservice.jwtsecurity.model.User;
import top.infoservice.jwtsecurity.service.AuthService;
import top.infoservice.jwtsecurity.service.UserService;
import top.infoservice.jwtsecurity.service.jwt.JwtAuthentication;
import top.infoservice.jwtsecurity.dto.JwtUser;

import javax.security.auth.message.AuthException;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final AuthService authService;

    @Autowired
    public RestController(AuthenticationManager authenticationManager, UserService userService, AuthService authService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.authService = authService;
    }

    @PostMapping("/auth/login")
    public ResponseEntity login(@RequestBody JwtRequest authDto) throws AuthException {
        try {
            String username = authDto.getUsername();
            String password = authDto.getPassword();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            User user = userService.findByUsername(username);
            if(user == null) {
                throw new UsernameNotFoundException("Пользователь " + username + " не найден");
            }
            JwtResponse token = authService.login(authDto);
            return ResponseEntity.ok(token);
        }catch (AuthenticationException e) {
            throw new BadCredentialsException("Неправильное имя пользователя или пароль");
        }

    }

    @GetMapping("auth/status")
    public ResponseEntity<JwtAuthentication> getNewAccessToken(@AuthenticationPrincipal JwtUser user) {
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<>(authService.getAuthInfo(), responseHeaders, HttpStatus.OK);
    }

    @PostMapping("auth/token/refresh")
    public ResponseEntity<JwtResponse> getNewRefreshToken(@RequestBody RefreshJwtRequest request) throws AuthException{
        final JwtResponse token = authService.refresh(request.getRefreshToken());
        return ResponseEntity.ok(token);
    }
}
