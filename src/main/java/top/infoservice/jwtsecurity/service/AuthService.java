package top.infoservice.jwtsecurity.service;


import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import top.infoservice.jwtsecurity.dto.JwtRequest;
import top.infoservice.jwtsecurity.dto.JwtResponse;
import top.infoservice.jwtsecurity.service.jwt.JwtAuthentication;
import top.infoservice.jwtsecurity.service.jwt.JwtProvider;
import top.infoservice.jwtsecurity.dto.JwtUser;

import javax.security.auth.message.AuthException;

@Service
public class AuthService {

    private final UserDetailsService userService;
    private final JwtProvider jwtProvider;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthService(UserDetailsService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.jwtProvider = jwtProvider;
        this.passwordEncoder = passwordEncoder;
    }

    public JwtResponse login(@NonNull JwtRequest authRequest) throws AuthException {
        final JwtUser user = (JwtUser) userService.loadUserByUsername(authRequest.getUsername());
        if(user == null) {
            new AuthException("Пользователь не найден");
        }

        if (passwordEncoder.matches(authRequest.getPassword(), user.getPassword())) {
            final String accessToken = jwtProvider.generateAccessToken(user);
            final String refreshToken = jwtProvider.generateRefreshToken(user);
            return new JwtResponse(accessToken, refreshToken);
        } else {
            throw new AuthException("Неправильный пароль");
        }
    }

    public JwtResponse refresh(@NonNull String refreshToken) throws AuthException {
        if (jwtProvider.validateRefreshToken(refreshToken)) {
            final Claims claims = jwtProvider.getRefreshClaims(refreshToken);
            final String login = claims.getSubject();
            final JwtUser user = (JwtUser)userService.loadUserByUsername(login);
            if(user == null) {
                new AuthException("Пользователь не найден");
            }

            final String accessToken = jwtProvider.generateAccessToken(user);
            final String newRefreshToken = jwtProvider.generateRefreshToken(user);
            return new JwtResponse(accessToken, newRefreshToken);
        }
        throw new AuthException("Невалидный JWT токен");
    }

    public JwtAuthentication getAuthInfo() {
        return (JwtAuthentication) SecurityContextHolder.getContext().getAuthentication();
    }

}
