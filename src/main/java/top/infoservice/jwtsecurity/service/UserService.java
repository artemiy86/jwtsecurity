package top.infoservice.jwtsecurity.service;

import top.infoservice.jwtsecurity.model.User;

public interface UserService {
    User findByUsername(String username);
}
