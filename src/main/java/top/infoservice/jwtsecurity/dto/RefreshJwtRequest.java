package top.infoservice.jwtsecurity.dto;

import lombok.Data;

@Data
public class RefreshJwtRequest {
    public String refreshToken;
}
