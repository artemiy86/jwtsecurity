package top.infoservice.jwtsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.infoservice.jwtsecurity.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
