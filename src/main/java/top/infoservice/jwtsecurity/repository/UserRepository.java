package top.infoservice.jwtsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.infoservice.jwtsecurity.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
