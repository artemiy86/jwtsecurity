package top.infoservice.jwtsecurity.model;

public enum Status {
    ACTIVE, NOT_ACTIVE
}
