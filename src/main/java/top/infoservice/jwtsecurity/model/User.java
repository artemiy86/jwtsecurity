package top.infoservice.jwtsecurity.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String username;
    private String password;

    private String firstName;
    private String lastName;
    private String email;

    private Status status;

    @ManyToMany
    private List<Role> roles;
}
